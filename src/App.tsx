import React from 'react';
import logo from './logo.svg';
import styled from 'styled-components';
import GlobalStyle from './styles/global';
import Container from './pages/Container';

function App() {
  return (
    <div>
      <GlobalStyle />
      <Container />
     </div>
  );
}

export default App;
