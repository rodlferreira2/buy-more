import  styled  from 'styled-components';


export const BoxPrincipal = styled.div`
    // display: flex;
    height: 100vh;
    align-items: center;
    background-color: #403F3F;
`;

export const WrapperTitle = styled.div`
    display: flex;
    justify-content: center;
    // padding: 100px 0;

`;	

export const Titulo = styled.h1`
color: #FFF;
margin-top: 100px;
`; 

export const WrapperTextArea = styled.div`
    display: flex;
    justify-content: center;
`;

export const TextArea = styled.input`
    border-radius: 16px;
    height: 81px;
    width: 648px;
    margin-top: 64px;
`;

export const WrapperButton = styled.div`
    display: flex;
    justify-content: center;
`;

export const Button = styled.button`
    background-color: #797979;
    color: #FFF;
    border-radius: 16px;
    height: 81px;
    width: 648px;
    margin-top: 60px;
`;