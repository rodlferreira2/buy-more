import React from "react";
import { BoxPrincipal, WrapperTitle, Titulo, TextArea, WrapperTextArea, WrapperButton, Button } from "./styles";



export default function Container() {

    const [text, setText] = React.useState("");
    const [list, setList] = React.useState<string[]>([]);
    const handleAddItem = () => {
        setList([...list, text]);
        setText("");
    }


    return (
        <>
            <BoxPrincipal>
                <WrapperTitle>
                    <Titulo>One List</Titulo>
                </WrapperTitle>
                <WrapperTextArea>
                    <TextArea placeholder="Digite alguma coisa" onChange={(e) => setText(e.target.value)}value={text} />
                </WrapperTextArea>
                <WrapperButton>
                    <Button onClick={handleAddItem}>Adicionar</Button>
                </WrapperButton>
                {list.map((listD) => (
                    <h1>{listD}</h1>
                ))}
            </BoxPrincipal>
        </>
    )
}

\\wsl$\Ubuntu20.04LTS\home\rodlferreira\desafios\desafio\src\pages\Container\index.tsx